#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import argparse
from PyQt5 import QtGui, QtCore, QtWidgets
import h5py
import numpy as np
import random

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.colors as colors

DEFAULT_CMAP = 'viridis'


class QtRresult(QtCore.QObject):
    def __init__(self, image, mask=None, center=None, parent=None):
        QtCore.QObject.__init__(self, parent)
        self.image = image
        self.mask = mask
        self.center = center


class LoadImageThread(QtCore.QThread):
    finished = QtCore.pyqtSignal(object)

    def __init__(self, callback, cxi_file, \
                 entry_id, imageset_id, image_id, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.cxi_file = cxi_file
        self.entry_id = entry_id
        self.imageset_id = imageset_id
        self.image_id = image_id
        self.finished.connect(callback)

    def run(self):
        self.finished.emit(QtRresult(*self.load_image()))

    def load_image(self):
        image_data = None
        mask = None
        center = None
        image_group_path = 'entry_%d/image_%d' % (self.entry_id, self.imageset_id)
        
        with h5py.File(self.cxi_file, 'r') as h5work:
            if image_group_path in h5work:
                image_group = h5work.get(image_group_path)
            else:
                return

            image_dataset = image_group.get('data')
            mask_dataset = image_group.get('mask')
            center_dataset = image_group.get('image_center')

            if image_dataset is None:
                sys.stderr.write('Wrong imageset path: %s\n' % image_group_path)
                return

            if len(image_dataset.shape) == 2:
                image_data = image_dataset[:]
            elif len(image_dataset.shape) == 3:
                image_data = image_dataset[self.image_id]
            else:
                sys.stderr.write('Image dataset has wrong shape: %s\n' % str(image_dataset.shape))
                return None

            if mask_dataset is None:
            	mask = None
            elif len(mask_dataset.shape) == 2:
                mask = mask_dataset[:]
            elif len(mask_dataset.shape) == 3:
                mask = mask_dataset[self.image_id]
            else:
                sys.stderr.write('Mask dataset has wrong shape: %s\n' % str(image_dataset.shape))
                return None

            if center_dataset is None:
            	center = None
            elif len(center_dataset.shape) == 1:
                center = center_dataset[:]
            elif len(center_dataset.shape) == 2:
                center = center_dataset[self.image_id]

            if np.iscomplex(image_data).any():
                image_data = np.real(image_data)

            return image_data, mask, center


class CXDIViewer(QtWidgets.QWidget):

    def __init__(self, cxi_file, parent=None):
        super(CXDIViewer, self).__init__(parent)

        self.cxi_file = cxi_file
        self.entry_id = 1
        self.number_of_entries = 1
        self.imageset_id = 1
        self.number_of_imagesets = 1
        self.image_id = 0
        self.number_of_images = 0
        self.current_image = None
        self.image_mask = None
        self.image_center = None
        self.im_mappable = None

        self.limits_min = None
        self.limits_max = None
        self.use_logscale = False
        self.show_mask = False
        self.show_image_center = False
        self.view_changed_flag = True

        self.colormap = plt.cm.get_cmap(DEFAULT_CMAP)

        self.cmap_mask = plt.get_cmap('Reds')  # Prepare colormap for mask
        alphas = np.linspace(0, 1, self.cmap_mask.N + 3)
        self.cmap_mask._init()
        self.cmap_mask._lut[:, -1] = alphas

        self.classification_data = None
        self.fixed_class = None

        self.initUI()
        self.load_cxi()
        self.load_classifications()

    def initUI(self):
        default_width = 1000
        default_height = 800
        self.setGeometry(400, 300, default_width, default_height)
        self.setWindowTitle('CXDI Viewer')
        # self.setWindowIcon(QtGui.QIcon('circle.png'))

        main_layout = QtWidgets.QHBoxLayout()
        control_layout = QtWidgets.QGridLayout()

        int_validator = QtGui.QIntValidator()
        row = 0

        # self.reload_btn = QtGui.QPushButton('Reload data')
        # self.reload_btn.clicked.connect(self.reload_data)
        # control_layout.addWidget(self.reload_btn, row, 0)

        self.progress_text = QtWidgets.QLabel('')
        control_layout.addWidget(self.progress_text, row, 0)
        row += 1

        entry_line_layout = QtWidgets.QHBoxLayout()

        self.entry_start_text = QtWidgets.QLabel('Entry:')
        entry_line_layout.addWidget(self.entry_start_text)

        self.entry_edit_ql = QtWidgets.QLineEdit()
        self.entry_edit_ql.returnPressed.connect(self.entry_edit)
        self.entry_edit_ql.setValidator(int_validator)
        self.entry_edit_ql.setAlignment(QtCore.Qt.AlignRight)
        entry_line_layout.addWidget(self.entry_edit_ql)

        self.entry_end_text = QtWidgets.QLabel('of 0')
        entry_line_layout.addWidget(self.entry_end_text)

        control_layout.addLayout(entry_line_layout, row, 0, 1, 2)
        row += 1

        self.entry_prev_btn = QtWidgets.QPushButton('Previous')
        self.entry_prev_btn.clicked.connect(self.previous_entry)
        control_layout.addWidget(self.entry_prev_btn, row, 0)

        self.entry_next_btn = QtWidgets.QPushButton('Next')
        self.entry_next_btn.clicked.connect(self.next_entry)
        control_layout.addWidget(self.entry_next_btn, row, 1)
        row += 1

        imageset_line_layout = QtWidgets.QHBoxLayout()

        self.imageset_start_text = QtWidgets.QLabel('Image set:')
        imageset_line_layout.addWidget(self.imageset_start_text)

        self.imageset_edit_ql = QtWidgets.QLineEdit()
        self.imageset_edit_ql.returnPressed.connect(self.imageset_edit)
        self.imageset_edit_ql.setValidator(int_validator)
        self.imageset_edit_ql.setAlignment(QtCore.Qt.AlignRight)
        imageset_line_layout.addWidget(self.imageset_edit_ql)

        self.imageset_end_text = QtWidgets.QLabel('of 0')
        imageset_line_layout.addWidget(self.imageset_end_text)

        control_layout.addLayout(imageset_line_layout, row, 0, 1, 2)
        row += 1

        self.imageset_prev_btn = QtWidgets.QPushButton('Previous')
        self.imageset_prev_btn.clicked.connect(self.previous_imageset)
        control_layout.addWidget(self.imageset_prev_btn, row, 0)

        self.imageset_next_btn = QtWidgets.QPushButton('Next')
        self.imageset_next_btn.clicked.connect(self.next_imageset)
        control_layout.addWidget(self.imageset_next_btn, row, 1)
        row += 1

        image_line_layout = QtWidgets.QHBoxLayout()

        self.image_start_text = QtWidgets.QLabel('Image:')
        image_line_layout.addWidget(self.image_start_text)

        self.image_edit_ql = QtWidgets.QLineEdit()
        self.image_edit_ql.returnPressed.connect(self.image_edit)
        self.image_edit_ql.setValidator(int_validator)
        self.image_edit_ql.setAlignment(QtCore.Qt.AlignRight)
        image_line_layout.addWidget(self.image_edit_ql)

        self.image_end_text = QtWidgets.QLabel('of 200000')
        image_line_layout.addWidget(self.image_end_text)

        control_layout.addLayout(image_line_layout, row, 0, 1, 2)
        row += 1

        self.image_prev_btn = QtWidgets.QPushButton('Previous')
        self.image_prev_btn.clicked.connect(self.previous_image)
        control_layout.addWidget(self.image_prev_btn, row, 0)

        self.image_next_btn = QtWidgets.QPushButton('Next')
        self.image_next_btn.clicked.connect(self.next_image)
        control_layout.addWidget(self.image_next_btn, row, 1)
        row += 1

        self.image_random_btn = QtWidgets.QPushButton('Random')
        self.image_random_btn.clicked.connect(self.random_image)
        control_layout.addWidget(self.image_random_btn, row, 0, 1, 2)
        row += 1

        control_layout.addWidget(self.hline(), row, 0, 1, 2)
        row += 1

        self.colormap_text = QtWidgets.QLabel('Colormap')
        control_layout.addWidget(self.colormap_text, row, 0, 1, 2)
        row += 1

        self.colormap_cb = QtWidgets.QComboBox()
        self.load_colormaps_to_cb()
        self.colormap_cb.currentIndexChanged.connect(self.change_colormap)
        control_layout.addWidget(self.colormap_cb, row, 0, 1, 2)
        row += 1

        limits_validator = QtGui.QRegExpValidator(QtCore.QRegExp('^([0-9])*$'))

        self.limits_min_text = QtWidgets.QLabel('Min value:')
        control_layout.addWidget(self.limits_min_text, row, 0)

        self.limits_min_ql = QtWidgets.QLineEdit()
        self.limits_min_ql.returnPressed.connect(self.limits_edit)
        control_layout.addWidget(self.limits_min_ql, row, 1)
        self.limits_min_ql.setValidator(limits_validator)
        row += 1

        self.limits_max_text = QtWidgets.QLabel('Max value:')
        control_layout.addWidget(self.limits_max_text, row, 0)

        self.limits_max_ql = QtWidgets.QLineEdit()
        self.limits_max_ql.returnPressed.connect(self.limits_edit)
        control_layout.addWidget(self.limits_max_ql, row, 1)
        self.limits_max_ql.setValidator(limits_validator)
        row += 1

        self.logscale_text = QtWidgets.QLabel('Logscale:')
        control_layout.addWidget(self.logscale_text, row, 0)

        self.logscale_check = QtWidgets.QCheckBox()
        self.logscale_check.setChecked(False)
        self.logscale_check.stateChanged.connect(self.logscale_changed)
        control_layout.addWidget(self.logscale_check, row, 1)
        row += 1

        control_layout.addWidget(self.hline(), row, 0, 1, 2)
        row += 1

        self.mask_text = QtWidgets.QLabel('Mask:')
        control_layout.addWidget(self.mask_text, row, 0)

        self.mask_check = QtWidgets.QCheckBox()
        self.mask_check.setChecked(False)
        self.mask_check.stateChanged.connect(self.mask_changed)
        control_layout.addWidget(self.mask_check, row, 1)
        row += 1

        self.image_center_text = QtWidgets.QLabel('Image center:')
        control_layout.addWidget(self.image_center_text, row, 0)

        self.image_center_check = QtWidgets.QCheckBox()
        self.image_center_check.setChecked(False)
        self.image_center_check.stateChanged.connect(self.image_center_changed)
        control_layout.addWidget(self.image_center_check, row, 1)
        row += 1

        control_layout.addWidget(self.hline(), row, 0, 1, 2)
        row += 1

        self.classify_text = QtWidgets.QLabel('Classification:')
        control_layout.addWidget(self.classify_text, row, 0)

        self.add_class_btn = QtWidgets.QPushButton('Add empty')
        self.add_class_btn.clicked.connect(self.add_new_classification)
        control_layout.addWidget(self.add_class_btn)

        row += 1

        self.classify_cb = QtWidgets.QComboBox()
        control_layout.addWidget(self.classify_cb, row, 0, 1, 2)
        row += 1

        self.class_stats_text = QtWidgets.QLabel('')
        self.class_stats_text.setWordWrap(True)
        control_layout.addWidget(self.class_stats_text, row, 0, 1, 2)

        row += 1

        self.fix_class_text = QtWidgets.QLabel('Fix class:')
        control_layout.addWidget(self.fix_class_text, row, 0)

        self.fix_class_cb = QtWidgets.QComboBox()
        control_layout.addWidget(self.fix_class_cb)

        row += 1

        self.assign_class_text = QtWidgets.QLabel('Assign class:')
        control_layout.addWidget(self.assign_class_text, row, 0, 1, 2)
        row += 1

        self.type_btn_layout = QtWidgets.QGridLayout()

        control_layout.addLayout(self.type_btn_layout, row, 0, 1, 2)
        row += 1

        spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        control_layout.addItem(spacer, row, 0)

        control_layout.setColumnStretch(0, 1)
        control_layout.setColumnStretch(1, 1)

        main_layout.addLayout(control_layout, 1)

        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)

        # set the layout
        plot_layout = QtWidgets.QVBoxLayout()
        plot_layout.addWidget(self.toolbar)
        plot_layout.addWidget(self.canvas)

        main_layout.addLayout(plot_layout, 4)

        self.setLayout(main_layout)

    def update_ui(self):
        self.entry_edit_ql.setText('%d' % self.entry_id)
        self.entry_end_text.setText('of %d' % self.number_of_entries)
        self.imageset_edit_ql.setText('%d' % self.imageset_id)
        self.imageset_end_text.setText('of %d' % self.number_of_imagesets)
        self.image_edit_ql.setText('%d' % (self.image_id + 1))
        self.image_end_text.setText('of %d' % self.number_of_images)

        self.limits_min_ql.setText(str(self.limits_min) if self.limits_min is not None else '')
        self.limits_max_ql.setText(str(self.limits_max) if self.limits_max is not None else '')

        active_entry = self.number_of_entries > 1
        self.entry_edit_ql.setEnabled(active_entry)
        self.entry_prev_btn.setEnabled(active_entry)
        self.entry_next_btn.setEnabled(active_entry)

        active_imageset = self.number_of_imagesets > 1
        self.imageset_edit_ql.setEnabled(active_imageset)
        self.imageset_prev_btn.setEnabled(active_imageset)
        self.imageset_next_btn.setEnabled(active_imageset)

        active_image = self.number_of_images > 1
        self.image_edit_ql.setEnabled(active_image)
        self.image_prev_btn.setEnabled(active_image)
        self.image_next_btn.setEnabled(active_image)
        self.image_random_btn.setEnabled(active_image)

        self.mask_check.setEnabled(self.image_mask is not None)
        self.image_center_check.setEnabled(self.image_center is not None)

    def load_colormaps_to_cb(self):
        for i, cmap_name in enumerate(sorted(plt.cm.cmap_d.keys())):
            self.colormap_cb.addItem(cmap_name)
            if cmap_name == DEFAULT_CMAP:
                def_index = i

        self.colormap_cb.setCurrentIndex(def_index)

    def load_classifications(self):
        try:
            self.classify_cb.currentIndexChanged.disconnect()
        except Exception:
            pass

        self.classify_cb.clear()
        self.classify_cb.addItem('No classification')
        image_group_path = 'entry_{:d}/image_{:d}'.format(self.entry_id, self.imageset_id)

        with h5py.File(self.cxi_file, 'r') as h5work:
            if image_group_path in h5work:
                image_group = h5work.get(image_group_path)
            else:
                return

            class_group = image_group.get('classification', None)
            if class_group is None:
                self.classification_data = None
                self.classify_cb.setEnabled(False)
            else:
                for dset_name in class_group.keys():
                    self.classify_cb.addItem(dset_name)
                    self.classify_cb.setEnabled(True)

        self.classification_data = None
        self.classify_cb.setCurrentIndex(0)
        self.update_class_stats()
        self.update_fix_class_cb()
        self.update_assign_class_layout()
        self.classify_cb.currentIndexChanged.connect(self.change_classification)

    def change_classification(self, i):
        dset_name = str(self.classify_cb.currentText())
        if dset_name == 'No classification':
            self.classification_data = None
        elif len(dset_name) > 0:
            class_data_path = 'entry_{:d}/image_{:d}/classification/{}'.format(self.entry_id, self.imageset_id,
                                                                               dset_name)

            with h5py.File(self.cxi_file, 'r') as h5work:
                if class_data_path in h5work:
                    self.classification_data = h5work.get(class_data_path)[:].astype(int)
                else:
                    self.classification_data = None
                    sys.stderr.write('Wrong classification path: %s\n' % class_data_path)

        self.update_class_stats()
        self.update_fix_class_cb()
        self.update_assign_class_layout()
        self.draw_image()

    def update_class_stats(self):
        if self.classification_data is not None:
            class_vals, class_counts = np.unique(self.classification_data, return_counts=True)
            self.class_stats_text.setText(' '.join(['{}:{}'.format(val, count) \
                                                    for val, count in zip(class_vals, class_counts)]))
        else:
            self.class_stats_text.setText('')

    def update_fix_class_cb(self):
        try:
            self.fix_class_cb.currentIndexChanged.disconnect()
        except Exception:
            pass

        self.fix_class_cb.clear()
        self.fix_class_cb.addItem('No fix')

        if self.classification_data is None:
            self.fix_class_cb.setEnabled(False)
        else:
            self.fix_class_cb.setEnabled(True)
            for val in np.unique(self.classification_data):
                self.fix_class_cb.addItem(str(val))

        self.fix_class_cb.setCurrentIndex(0)
        self.fixed_class = None
        self.fix_class_cb.currentIndexChanged.connect(self.change_fix_class_cb)
        pass

    def change_fix_class_cb(self):
        class_str = str(self.fix_class_cb.currentText())
        if class_str == 'No fix':
            self.fixed_class = None
        else:
            self.fixed_class = int(class_str)
            self.set_current_image_id(self.image_id)

        self.update_class_stats()
        self.draw_image()

    def add_new_classification(self):
        new_name, is_ok = QtGui.QInputDialog.getText(self, 'Create new classification', 'Enter classification name')

        if is_ok:
            image_data_path = 'entry_{:d}/image_{:d}/'.format(self.entry_id, self.imageset_id)

            with h5py.File(self.cxi_file, 'a') as h5work:
                if image_data_path in h5work:
                    image_group = h5work.get(image_data_path)
                else:
                    sys.stderr.write('Wrong image path: %s\n' % image_data_path)

                class_group_name = 'classification'

                if class_group_name in image_group.keys():
                    class_group = image_group[class_group_name]
                else:
                    class_group = image_group.create_group(class_group_name)

                if new_name in class_group.keys():
                    sys.stderr.write('Classification with name "{}" already exists\n'.format(new_name))
                    return

                class_group.create_dataset(new_name, shape=(self.number_of_images,), dtype=np.int, fillvalue=0)

            self.load_classifications()

    def update_assign_class_layout(self):
        # Clear layout
        for i in reversed(range(self.type_btn_layout.count())):
            self.type_btn_layout.itemAt(i).widget().deleteLater()

        if self.classification_data is None:
            self.type_btn_layout.setEnabled(False)
        else:
            classes = np.unique(self.classification_data)

            if 0 not in classes:
                classes = list(classes)
                classes.append(0)
                classes = sorted(classes)

            i = 0
            for cl in classes:
                type_btn = QtWidgets.QPushButton(str(cl))
                type_btn.clicked.connect(lambda evt, c=cl: self.assign_class(c))
                self.type_btn_layout.addWidget(type_btn, i//2, i%2)
                i += 1

            # Add one more to allow create new class
            new_class = classes[-1] + 1
            type_btn = QtWidgets.QPushButton('+{:d}'.format(new_class))
            type_btn.clicked.connect(lambda evt, c=new_class: self.assign_class(c))
            self.type_btn_layout.addWidget(type_btn, i//2, i%2)

    def assign_class(self, c):
        dset_name = str(self.classify_cb.currentText())
        class_data_path = 'entry_{:d}/image_{:d}/classification/{}'.format(self.entry_id, self.imageset_id,
                                                                           dset_name)

        with h5py.File(self.cxi_file, 'a') as h5work:
            if class_data_path in h5work:
                class_data = h5work.get(class_data_path)
                class_data[self.image_id] = c
            else:
                sys.stderr.write('Wrong classification path: %s\n' % class_data_path)

        self.classification_data[self.image_id] = c

        self.update_class_stats()
        self.update_assign_class_layout()
        self.update_fix_class_cb()
        self.draw_image()

    def hline(self):
        qtline = QtWidgets.QFrame()
        qtline.setFrameShape(QtWidgets.QFrame.HLine)
        qtline.setFrameShadow(QtWidgets.QFrame.Sunken)
        return qtline

    def change_colormap(self, i):
        cmap_name = str(self.colormap_cb.currentText())
        self.colormap = plt.cm.get_cmap(cmap_name)
        self.draw_image()

    def previous_entry(self, event):
        self.set_entry(self.entry_id - 1)
        self.set_imageset(1)
        self.set_current_image(1)

    def next_entry(self, event):
        self.set_entry(self.entry_id + 1)
        self.set_imageset(1)
        self.set_current_image(1)

    def entry_edit(self):
        self.set_entry(int(self.entry_edit_ql.text()))
        self.set_imageset(1)
        self.set_current_image(1)

    def previous_imageset(self, event):
        self.set_imageset(self.imageset_id - 1)
        self.set_current_image(1)

    def next_imageset(self, event):
        self.set_imageset(self.imageset_id + 1)
        self.set_current_image(1)

    def imageset_edit(self):
        self.set_imageset(int(self.imageset_edit_ql.text()))
        self.set_current_image(1)

    def previous_image(self, event):
        self.set_current_image_id(self.image_id - 1)

    def next_image(self, event):
        self.set_current_image_id(self.image_id + 1)

    def random_image(self, event):
        random_num = random.randint(1, self.number_of_images)
        self.set_current_image(random_num)

    def image_edit(self):
        self.set_current_image(int(self.image_edit_ql.text()))

    def logscale_changed(self):
        self.use_logscale = self.logscale_check.isChecked()
        self.view_changed_flag = True
        self.draw_image()

    def mask_changed(self):
        self.show_mask = self.mask_check.isChecked()
        self.view_changed_flag = True
        self.draw_image()

    def image_center_changed(self):
        self.show_image_center = self.image_center_check.isChecked()
        self.view_changed_flag = True
        self.draw_image()

    def limits_edit(self):
        if len(self.limits_min_ql.text()) == 0:
            self.limits_min = None
        else:
            self.limits_min = int(self.limits_min_ql.text())

        if len(self.limits_max_ql.text()) == 0:
            self.limits_max = None
        else:
            self.limits_max = int(self.limits_max_ql.text())
        self.draw_image()

    def load_cxi(self):
        with h5py.File(self.cxi_file, 'r') as h5work:
            self.number_of_entries = h5work.get('number_of_entries', 1)

            in_file_entries = sum([1 for k in h5work.keys() if k[:6] == 'entry_'])
            if in_file_entries != self.number_of_entries:
                self.show_error('Num of entries is wrong, %d != %d\n' % (self.number_of_entries, \
                                                                         in_file_entries))
                self.number_of_entries = in_file_entries

    def set_entry(self, entry_id):
        if self.number_of_entries < 1:
            self.number_of_imagesets = 0
            self.number_of_images = 0
        else:
            while entry_id < 1:
                entry_id += self.number_of_entries
            while entry_id > self.number_of_entries:
                entry_id -= self.number_of_entries

            self.entry_id = entry_id

            entry_path = 'entry_%d' % self.entry_id
            with h5py.File(self.cxi_file, 'r') as h5work:
                if entry_path in h5work.keys():
                    entry_data = h5work[entry_path]
                else:
                    self.show_error('No %s in work file' % entry_path)
                    return

                num_imagesets = 0
                for k in entry_data.keys():
                    if k.startswith('image_'):
                        num_imagesets = max(num_imagesets, int(k[6:]))

                self.number_of_imagesets = num_imagesets

        self.update_ui()

    def set_imageset(self, imageset_id):
        if self.number_of_imagesets < 1:
            self.number_of_images = 0
        else:
            while imageset_id < 1:
                imageset_id += self.number_of_imagesets
            while imageset_id > self.number_of_imagesets:
                imageset_id -= self.number_of_imagesets

            self.imageset_id = imageset_id

            imageset_path = 'entry_%d/image_%d/data' % (self.entry_id, self.imageset_id)

            with h5py.File(self.cxi_file, 'r') as h5work:
                if imageset_path in h5work:
                    image_dataset = h5work.get(imageset_path)
                else:
                    image_dataset = None

                if image_dataset is None:
                    self.number_of_images = 0
                elif len(image_dataset.shape) == 2:
                    self.number_of_images = 1
                elif len(image_dataset.shape) == 3:
                    self.number_of_images = image_dataset.shape[0]
                else:
                    self.show_error('Image dataset has wrong shape: %s' % str(image_dataset.shape))
                    return

        self.load_classifications()
        self.update_ui()

    def set_current_image(self, image_num):
        self.set_current_image_id(image_num - 1)  # num starts from 1, id starts from 0

    def set_current_image_id(self, image_id):
        if self.number_of_images >= 1:

            if self.fixed_class is not None \
                    and self.classification_data is not None \
                    and self.classification_data[image_id] != self.fixed_class:
                # We search for the first image of fixed class after current image_id
                # and set image_id to that image
                mask = (self.classification_data == self.fixed_class)

                if image_id == self.image_id - 1:
                    # We need previous hit, not next one
                    rolled_mask = np.roll(mask, -1 * image_id - 1)
                    image_id -= np.argmax(rolled_mask[::-1])
                else:
                    # Normal behaviour, search for next hit
                    rolled_mask = np.roll(mask, -1 * image_id)
                    image_id += np.argmax(rolled_mask)

            while image_id < 0:
                image_id += self.number_of_images
            while image_id >= self.number_of_images:
                image_id -= self.number_of_images

            self.image_id = image_id

            self.start_data_load()

    def start_data_load(self):
        self.set_loading(True)
        self.thread = LoadImageThread(self.finish_data_load, self.cxi_file, \
                                      self.entry_id, self.imageset_id, self.image_id)
        self.thread.start()

    def finish_data_load(self, qt_result):
        self.current_image = qt_result.image
        self.image_mask = qt_result.mask
        self.image_center = qt_result.center
        if self.image_mask is not None:
            self.current_image[self.image_mask > 0] = 0
        self.draw_image()
        self.update_ui()
        self.set_loading(False)

    def set_loading(self, is_loading=True):
        loading_state_string = 'Loading...' if is_loading else ''
        self.progress_text.setText(loading_state_string)

    def draw_image(self):
        if self.limits_min is None:
            vmin = np.amin(self.current_image)
        else:
            vmin = self.limits_min

        if self.limits_max is None:
            vmax = np.amax(self.current_image)
        else:
            vmax = self.limits_max

        if vmin <= 0 and self.use_logscale:
            vmin = 0.999

        if self.use_logscale:
            norm = colors.LogNorm(vmin=vmin, vmax=vmax)
        else:
            norm = colors.Normalize(vmin=vmin, vmax=vmax)

        cmap = self.colormap
        cmap.set_under(color='white')

        if self.im_mappable is None or self.view_changed_flag:
            self.figure.clf()
            axes = self.figure.add_subplot(111)
            self.figure.tight_layout()
            self.im_mappable = axes.imshow(self.current_image, cmap=cmap, norm=norm, interpolation='nearest')

            if self.image_mask is not None and self.show_mask:
                axes.imshow(self.image_mask != 0, cmap=self.cmap_mask, interpolation='nearest')

            if self.image_center is not None and self.show_image_center:
                for m in [20, 40, 60]:
                    axes.add_patch(
                        plt.Circle((self.image_center[0], self.image_center[1]),
                                   m, linewidth=1, edgecolor='r', facecolor='none')
                        )
                axes.plot(self.image_center[0], self.image_center[1], 'r+')

            divider = make_axes_locatable(axes)
            cax = divider.append_axes("right", size="5%", pad=0.05)
            self.colorbar = plt.colorbar(self.im_mappable, cax=cax)

            # if self.classification_data is not None:
            #     cl_x, cl_y = 0.05, 0.95
            #     class_value = self.classification_data[self.image_id]
            #     axes.text(cl_x, cl_y, str(class_value), \
            #               horizontalalignment='left', verticalalignment='top', \
            #               transform=axes.transAxes, fontsize=32, color='k', \
            #               bbox=dict(boxstyle='round', facecolor='white', alpha=1))

            # Due to bug in matplotlib we cannot switch between norm and lognorm on the fly
            self.view_changed_flag = False
        else:
            self.im_mappable.set_array(self.current_image)
            self.im_mappable.set_clim(vmin=vmin, vmax=vmax)
            self.colorbar.draw_all()

        self.canvas.draw()

    def show_error(self, error_line):
        sys.stderr.write(error_line + '\n')


def main():
    parser = argparse.ArgumentParser(description='CXDI viewer')
    parser.add_argument("cxi_file", help="Working file")
    parser.add_argument("-e", "--entry", dest="entry_id", \
                        help="Number of entry to show", type=int, default=1)
    parser.add_argument("-s", "--set", dest="imageset_id", \
                        help="Number of image set", type=int, default=1)
    parser.add_argument("-i", "--image", dest="image_num", \
                        help="Number of image to show", type=int, default=1)
    parser_args = parser.parse_args()

    if not os.path.exists(parser_args.cxi_file) or not os.path.isfile(parser_args.cxi_file):
        parser.error('Working file do not exist')

    cxi_file = os.path.abspath(parser_args.cxi_file)
    entry_id = parser_args.entry_id
    imageset_id = parser_args.imageset_id
    image_num = parser_args.image_num

    app = QtWidgets.QApplication(sys.argv)
    viewer_window = CXDIViewer(cxi_file)
    viewer_window.load_cxi()
    viewer_window.set_entry(entry_id)
    viewer_window.set_imageset(imageset_id)
    viewer_window.set_current_image(image_num)
    viewer_window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
